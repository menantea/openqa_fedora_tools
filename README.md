openQA Fedora additional tools
==============================

This repository contains additional tools for testing Fedora with
[openQA](http://os-autoinst.github.io/openQA/). For actual
openQA tests for Fedora, see [this repository](https://bitbucket.org/rajcze/openqa_fedora).

Installation of openQA
----------------------
There are currently two ways to install openQA to do Fedora testing.

1. Install openQA from packages on one or more Fedora systems - see `InstallGuide.md`
2. Use any Linux distribution and use Docker - see `docker/README.md`

The Docker option is intended only for development and is not recommended for
deployment.

Installation of fedora-openqa-schedule
--------------------------------------

After openQA is installed, you will probably want to set up
fedora-openqa-schedule, a tool for downloading ISOs, scheduling tests and
reporting results to the wiki. Otherwise you will have to find some other way
to generate test jobs. If you have deployed openQA in docker containers,
note that fedora-openqa-schedule should be installed and configured on the
**host**, not in any of the containers.

1. Install packaged dependencies:

        dnf install python-setuptools python-six python-requests python-fedfind python-wikitcms python-fedmsg-core python-resultsdb_api

2. Check out some a dependency from git and install it:

        git clone https://github.com/os-autoinst/openQA-python-client.git
        cd openQA-python-client; sudo python setup.py install
        cd ..

3. Set up a python-wikitcms credentials file. This file contains a FAS
username and password which will be used when reporting results to the
wiki. See the `python-wikitcms` documentation for more details on this.
Ideally this should be a dedicated account for the purpose of reporting
test results. This step can be skipped if you do not want to report
results to the wiki.

        sudo su
        mkdir /etc/fedora
        echo "fas_username fas_password" > /etc/fedora/credentials
        chmod 0600 /etc/fedora/credentials

5. [docker] If you run openQA in Docker, you have to set up credentials for
openQA API access on the host system. Create file `/etc/openqa/client.conf`
with following content:

        [localhost:8080]
        key = KEY
        secret = SECRET

    where `KEY` and `SECRET` are values from http://localhost:8080/api_keys you
    have created during openQA installation. You can run:

        docker exec openqa_webui cat /data/conf/client.conf

    which displays both values. It is a good idea to make this file readable
    only by user(s) which will run `fedora-openqa-schedule` jobs: any user
    who can read this file can perform any openQA API requests they like.

6. Get openqa-fedora-schedule (and associated tools) if you do not already
have them, and install:

        git clone https://bitbucket.org/rajcze/openqa_fedora_tools
        cd openqa_fedora_tools/scheduler; sudo python setup.py install
        cd ../..

7. Enable fedmsg consumer:

    To run openQA jobs whenever a compose completes, you must install and
    enable fedmsg-hub. On Fedora:

        sudo dnf install fedmsg-hub
        sudo systemctl enable fedmsg-hub.service

    Now enable the consumer. Create a file `/etc/fedmsg.d/openqa_consumer.py`
    with the contents:

        config = {
            'fedora_openqa_schedule.consumer.enabled': True,
        }

    Now start fedmsg-hub (or restart it if it's already running):

        sudo systemctl start fedmsg-hub.service

    In the Docker configuration you should probably set up the fedmsg
    consumer on the host system, ensuring you have done step 5 above.
    The fedmsg consumer will run as user 'fedmsg', so you need to give
    this user access to the wikitcms and openQA credentials:

        sudo chown root.fedmsg /etc/fedora/credentials
        sudo chown root.fedmsg /etc/openqa/client.conf
        sudo chmod 0640 /etc/fedora/credentials
        sudo chmod 0640 /etc/openqa/client.conf

You can run `fedora-openqa-schedule` manually to run tests on a specified
Fedora compose/release location. See the command's help for further details.
