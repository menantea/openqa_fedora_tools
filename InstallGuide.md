OpenQA install guide
====================

For Fedora 23+

Install the required packages:

        sudo dnf install openqa openqa-httpd openqa-worker

Configure httpd:

        cd /etc/httpd/conf.d/
        cp openqa.conf.template openqa.conf
        cp openqa-ssl.conf.template openqa-ssl.conf


Configure OpenQA WebUI by editing `/etc/openqa/openqa.ini`:

        [global]
        branding=plain
        download_domains = fedoraproject.org opensuse.org
        [auth]
        method = Fake

Start the Services (consider also running `enable` if you want persistence):

        systemctl start httpd
        systemctl start openqa-gru
        systemctl start openqa-webui

Create non-temporary API keys in the Web Interface at http://localhost click `Login`, then `Manage API Keys` and create new keyset.
Edit `/etc/openqa/client.conf`:

        [localhost]
        key = insert key from web api
        secret = insert secret from web api

Start the Worker:

        systemctl start openqa-worker@1

Check that it's active in the WebUI's admin interface. To start more workers, change the number after the `@` sign in the above command.

Grab the tests, and initialize the database:

        cd /var/lib/openqa/tests/
        git clone https://bitbucket.org/rajcze/openqa_fedora fedora
        chown -R geekotest:geekotest fedora
        cd fedora
        ./templates

To run the tests, you will also need to provide some disk images:

        git clone http://bitbucket.org/rajcze/openqa_fedora_tools.git ~/openqa_fedora_tools
        dnf install libguestfs-tools libguestfs-xfs python3-fedfind python3-libguestfs libvirt-daemon-config-network libvirt-python3 virt-install
        mkdir -p /var/lib/openqa/factory/hdd
        cd /var/lib/openqa/factory/hdd/
        ~/openqa_fedora_tools/tools/createhdds.py all
        chown geekotest *

[optional] go to OpenQA web interface, go to "admin" -> "Test suites".
Set `REPOSITORY_GRAPHICAL` and `REPOSITORY_VARIATION` to your nearest mirror
according to this list: https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-23&arch=x86_64,
excluding `/23/x86_64/os/` at the end of url. For example if your nearest mirror
is http://ftp.linux.cz/pub/linux/fedora/linux/development/23/x86_64/os/, set
`REPOSITORY_GRAPHICAL` and `REPOSITORY_VARIATION` to http://ftp.linux.cz/pub/linux/fedora/linux/development.

At this point most of the tests should work. However, a few of the tests require two or more workers to be able to communicate with each other (e.g. the FreeIPA tests, where one job sets up a server, and other jobs set up clients and try to connect to the server). For these tests to work, you must do some quite complex software-defined networking configuration on the worker host(s). Instructions for this are in [AdvancedNetwork.md](AdvancedNetwork.md).
