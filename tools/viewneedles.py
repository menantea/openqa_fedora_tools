#!/usr/bin/env python
import pygame
import sys
import os.path
import glob
import json
from pygame.locals import *

WIDTH = 1024
HEIGHT = 768
TEXTWIDTH = 24
RES = (WIDTH, HEIGHT + 10 + TEXTWIDTH + 10)

def load_areas_tags(img, jsons):
    if img in jsons.keys():
        f = open(jsons[img], "r")
        img_json = f.read()
        f.close()
        parsed = json.loads(img_json)
        return parsed["area"], parsed["tags"]
    else:
        return [], []

def rerender_window(wo, img_files, index, jsons):
    img = img_files[index]

    imageSurfaceObj = pygame.image.load(img)
    pygame.display.set_caption(os.path.basename(img) + " [%d/%d]" % (index + 1, len(img_files)))
    areas, tags = load_areas_tags(img, jsons)
    wo.fill((0, 0, 0))

    wo.blit(imageSurfaceObj, (0, 0))
    for area in areas:
        pos = (area["xpos"], area["ypos"], area["width"], area["height"])
        pygame.draw.rect(wo, pygame.Color(255, 0, 0), pos, 3)

    if pygame.font:
        font = pygame.font.Font(None, TEXTWIDTH)
        content = []
        for tag in tags:
            if font and not tag.startswith("ENV-"):
                content.append(tag)
        text = font.render(", ".join(content), 1, (255, 255, 255))
        textpos = (10, HEIGHT + 10)
        wo.blit(text, textpos)

def get_images_jsons(arg):
    if os.path.isdir(arg[0]) and len(arg) == 1:
        img_files = glob.glob(os.path.join(arg[0], "*.png"))
        json_files = glob.glob(os.path.join(arg[0], "*.json"))
    elif os.path.isdir(arg[0]) and len(arg) == 2:
        all_json_files = glob.glob(os.path.join(sys.argv[1], "*.json"))
        img_files = []
        json_files = []
        for jf in all_json_files:
            f = open(jf, "r")
            img_json = f.read()
            f.close()
            parsed = json.loads(img_json)
            if arg[1] in parsed["tags"]:
                root, ext = os.path.splitext(jf)
                img_files.append(root + ".png")
                json_files.append(jf)
    else:
        img_files = []
        json_files = []
        for f in arg:
            root, ext = os.path.splitext(f)
            if ext == ".png":
                img_files.append(f)
                json_files.append(root + ".json")
    img_files.sort()
    return img_files, json_files

def main_loop():
    if len(sys.argv) == 1:
        print "%s directory (tag) | file1 file2 file3..." % sys.argv[0]
        sys.exit()

    img_files, json_files = get_images_jsons(sys.argv[1:])

    if len(img_files) == 0:
        print "no images found"
        sys.exit(0)

    jsons = {}
    for img in img_files:
        json_name = os.path.splitext(img)[0] + ".json"
        if json_name in json_files:
            jsons[img] = json_name

    pygame.init()
    fpsClock = pygame.time.Clock()

    windowSurfaceObj = pygame.display.set_mode(RES)
    pygame.display.set_caption("OpenQA needles viewer")

    index = 0
    rerender_window(windowSurfaceObj, img_files, index, jsons)

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:
                if event.key == K_LEFT:
                    index = index - 1 if index > 0 else len(img_files) - 1
                    rerender_window(windowSurfaceObj, img_files, index, jsons)
                elif event.key == K_RIGHT:
                    index = index + 1 if index + 1 < len(img_files) else 0
                    rerender_window(windowSurfaceObj, img_files, index, jsons)
                elif event.key == K_ESCAPE or event.key == K_q:
                    pygame.event.post(pygame.event.Event(QUIT))

        pygame.display.update()
        fpsClock.tick(60)

if __name__ == "__main__":
    main_loop()
