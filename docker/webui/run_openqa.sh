#!/bin/bash

_term() {
  kill -TERM "$child" 2>/dev/null
}

trap _term SIGTERM

# prepare environment
/usr/share/openqa/script/upgradedb --user geekotest --upgrade_database

# run services
dbus-daemon --system --fork
su -s /bin/bash -c '/usr/share/openqa/script/openqa-scheduler' geekotest &
su -s /bin/bash -c '/usr/share/openqa/script/openqa-websockets' geekotest &
/usr/sbin/httpd
su -s /bin/bash -c '/usr/share/openqa/script/openqa prefork -m production --proxy' geekotest &

child=$!
wait "$child"

kill -TERM `cat /run/httpd/httpd.pid`
kill -TERM `cat /run/dbus/messagebus.pid`
rm -rf /run/dbus/*
