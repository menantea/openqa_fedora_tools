FROM fedora:23
MAINTAINER Jan Sedlak <jsedlak@redhat.com>, Josef Skladanka <jskladan@redhat.com>

RUN dnf install -y 'dnf-command(copr)' && \
    dnf copr -y enable adamwill/openQA && \
    dnf install -y openqa libselinux-python git json_diff libselinux-utils \
        libsemanage-python 'perl(Class::DBI::Pg)' 'perl(DateTime::Format::Pg)'

# TEMP: add create_admin script (pending upstream merge)
# https://github.com/os-autoinst/openQA/pull/444
ADD create_admin /var/lib/openqa/script/create_admin

ADD openqa.conf /etc/httpd/conf.d/openqa.conf
ADD run_openqa.sh /root/
ADD scripts /scripts
RUN chmod -R 777 /scripts

# set-up shared data and configuration
RUN rm -rf /etc/openqa/openqa.ini /etc/openqa/client.conf \
      /var/lib/openqa/share/factory /var/lib/openqa/share/tests \
      /var/lib/openqa/db/db.sqlite /var/lib/openqa/testresults && \
    chmod +x /root/run_openqa.sh && \
    mkdir -p /run/dbus && \
    ln -s /data/conf/openqa.ini /etc/openqa/openqa.ini && \
    ln -s /data/conf/client.conf /etc/openqa/client.conf && \
    ln -s /data/factory /var/lib/openqa/share/factory && \
    ln -s /data/tests /var/lib/openqa/share/tests && \
    ln -s /data/testresults /var/lib/openqa/testresults && \
    ln -s /data/db/db.sqlite /var/lib/openqa/db/db.sqlite

EXPOSE 80
CMD ["/root/run_openqa.sh"]
